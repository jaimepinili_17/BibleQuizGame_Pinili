var countCorrect = 0;
var countWrong = 0;

//init pages
$('#Q22').hide();
$('#Q23').hide();
$('#Q24').hide();
$('#Q25').hide();
$('#Q26').hide();
$('#Q27').hide();
$('#Q28').hide();
$('#Q29').hide();
$('#Q30').hide();
$('#tally2').hide();
$('#retake').hide();
$('#fin').hide();

function answer21(ans) {

    if (ans === 'ans') {
        console.log("Correct");
        $('#Q21').hide();
        $('#Q22').show();
        countCorrect++;
        $('#correct').html(countCorrect);

        
    }else {
        $('#Q21').hide();
        $('#Q22').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
    
    }

}

function answer22(ans) {

    if (ans === 'ans') {
        console.log("Correct");
        $('#Q22').hide();
        $('#Q23').show();
        countCorrect++;
        $('#correct').html(countCorrect);


        
    }else {
        $('#Q22').hide();
        $('#Q23').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
    
    }

}

function answer23(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q23').hide();
        $('#Q24').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q23').hide();
        $('#Q24').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer24(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q24').hide();
        $('#Q25').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q24').hide();
        $('#Q25').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer25(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q25').hide();
        $('#Q26').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q25').hide();
        $('#Q26').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer26(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q26').hide();
        $('#Q27').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q26').hide();
        $('#Q27').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer27(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q27').hide();
        $('#Q28').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q27').hide();
        $('#Q28').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer28(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q28').hide();
        $('#Q29').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q28').hide();
        $('#Q29').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer29(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q29').hide();
        $('#Q30').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q29').hide();
        $('#Q30').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer30(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q30').hide();
        $('#tally2').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q30').hide();
        $('#tally2').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }

    if(countCorrect >= 7){
        $('#retake').hide();
        $('#fin').show();
    }else{
        $('#retake').show();
        $('#fin').hide();
    }



console.log(countCorrect);
console.log(countWrong);
}