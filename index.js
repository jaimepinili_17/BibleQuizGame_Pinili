var countCorrect = 0;
var countWrong = 0;

//init pages
$('#Q2').hide();
$('#Q3').hide();
$('#Q4').hide();
$('#Q5').hide();
$('#Q6').hide();
$('#Q7').hide();
$('#Q8').hide();
$('#Q9').hide();
$('#Q10').hide();
$('#tally').hide();
$('#retake').hide();
$('#proceed').hide();



function answer1(ans) {

    if (ans === 'ans') {
        console.log("Correct");
        $('#Q1').hide();
        $('#Q2').show();
        countCorrect++;
        $('#correct').html(countCorrect);

        
    }else {
        $('#Q1').hide();
        $('#Q2').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
    
    }

}

function answer2(ans) {

    if (ans === 'ans') {
        console.log("Correct");
        $('#Q2').hide();
        $('#Q3').show();
        countCorrect++;
        $('#correct').html(countCorrect);


        
    }else {
        $('#Q2').hide();
        $('#Q3').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
    
    }

}

function answer3(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q3').hide();
        $('#Q4').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q3').hide();
        $('#Q4').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer4(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q4').hide();
        $('#Q5').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q4').hide();
        $('#Q5').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer5(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q5').hide();
        $('#Q6').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q5').hide();
        $('#Q6').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer6(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q6').hide();
        $('#Q7').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q6').hide();
        $('#Q7').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer7(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q7').hide();
        $('#Q8').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q7').hide();
        $('#Q8').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer8(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q8').hide();
        $('#Q9').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q8').hide();
        $('#Q9').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer9(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q9').hide();
        $('#Q10').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q9').hide();
        $('#Q10').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }
    
}

function answer10(ans) {
    
    if (ans === 'ans') {
        console.log("Correct");
        $('#Q10').hide();
        $('#tally').show();
        countCorrect++;
        $('#correct').html(countCorrect);
    
    
            
    }else {
        $('#Q10').hide();
        $('#tally').show();
        console.log("Wrong");
        countWrong++;
        $('#wrong').html(countWrong);
        
    }

    if(countCorrect >= 7){
        $('#retake').hide();
        $('#proceed').show();
    }else{
        $('#retake').show();
        $('#proceed').hide();
    }


console.log(countCorrect);
console.log(countWrong);
}